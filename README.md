This is a very simple container build. Take upstream Wordpress, add [SSMTP](https://wiki.archlinux.org/title/SSMTP), and a script which configures SSMTP from environment variables on container run.

This allows me to send mail from Wordpress sites, by using the container host as a relay. It's a bit noddy.
