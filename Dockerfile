FROM wordpress:php8.0-fpm-alpine

RUN set -eux; \
    apk add ssmtp

RUN cat /etc/ssmtp/ssmtp.conf

# Copy a script that runs at container start, to configure ssmtp

COPY ssmtp-docker-entrypoint.sh /usr/local/bin/

ENTRYPOINT ["/usr/local/bin/ssmtp-docker-entrypoint.sh"]
